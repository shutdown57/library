-- all books
select * from books

-- all employees
select * from employees

-- all tags
select * from tags

-- all members
select * from members

-- all authors
select * from authors

-- all publishers
select * from publishers

-- all categories
select * from categories

-- translators
select * from translators

-- get book by title
select books.*
from books
where books.title = 'Land of Lisp'

-- get books by category_id
select books.*
from books
where books.category_id = 3

-- get books by publishers
select books.*
from books
where books.publisher_id = 1

-- get books by translator_id
select books.*
from books
where books.translator_id = 1

-- categories books
select  categories.id,
        categories.name, books.title
from categories
        left join books
            on categories.id = books.category_id
order by categories.id

-- publishers books
select  publishers.id,
        publishers.name, books.title
from publishers
        left join books
            on publishers.id = books.publisher_id
order by publishers.id

-- taken book by member name
select books.title
from books
inner join taken_books on books.id = taken_books.book_id
inner join members on taken_books.member_id = members.id
where members.first_name = 'hamid'

--  who taken book by name
select members.id, members.first_name, members.last_name
from members
inner join taken_books on members.id = taken_books.book_id
inner join books on taken_books.member_id = books.id
where books.title = 'Land of Lisp'

-- get tags name by book title
select tags.name
from tags
inner join book_tag on tags.id = book_tag.tag_id
inner join books on book_tag.book_id = books.id
where books.title = 'Clojure Cookbook'
