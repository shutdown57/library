-- Create database
create database library;

-- Tables
-- Books table
create table books (
  id INT primary key auto_increment,
  title varchar(30) not null,
  description text,
  circulation varchar(10),
  isbn varchar(40) unique,
  language varchar(20),
  price int(7),
  author_id int,
  publisher_id int,
  translator_id int,
  category_id int,
  published_at datetime default current_timestamp,
  created_at datetime default current_timestamp,
  updated_at datetime
);

-- Authors table
create table authors (
  id INT primary key auto_increment,
  first_name varchar(20),
  last_name varchar(30),
  sex varchar(1),
  created_at datetime default current_timestamp
);

-- Publishers table
create table publishers (
  id INT primary key auto_increment,
  name varchar(50),
  address text,
  phone_number varchar(11) unique,
  email varchar(120) unique,
  website varchar(120) unique,
  created_at datetime default current_timestamp
);

-- Transaction table
create table translators (
  id INT primary key auto_increment,
  first_name varchar(20),
  last_name varchar(30),
  sex varchar(1),
  person_id varchar(10) unique,
  created_at datetime default current_timestamp
);

-- Book_Tag middle table
create table book_tag (
  id INT auto_increment primary key,
  book_id INT,
  tag_id INT,
  created_at datetime default current_timestamp
);

-- Tags table
create table tags (
  id INT primary key auto_increment,
  name varchar(20) not null unique,
  created_at datetime default current_timestamp
);

-- Taken_Books middle table
create table taken_books (
  id INT primary key auto_increment,
  book_id int,
  member_id int,
  created_at datetime default current_timestamp
);

-- Members table
create table members (
  id INT primary key auto_increment,
  first_name varchar(20),
  last_name varchar(30),
  sex varchar(2),
  person_id varchar(11) unique,
  address text,
  phone_number varchar(12),
  mobile_number varchar(12) unique,
  email varchar(120) unique,
  birthday datetime,
  updated_at datetime,
  created_at datetime default current_timestamp
);

-- Categories table
create table categories (
  id INT primary key auto_increment,
  name varchar(50) not null unique,
  created_at datetime default current_timestamp
);

-- Employees table
create table employees (
  id INT primary key auto_increment,
  first_name varchar(20),
  last_name varchar(30),
  sex varchar(1),
  person_id varchar(10) unique,
  address text,
  phone_number varchar(12),
  mobile_number varchar(12) unique,
  email varchar(120) unique,
  salary int(8),
  birthday datetime,
  updated_at datetime,
  created_at datetime default current_timestamp
);

-- foreign keys
alter table taken_books add foreign key (member_id) references members(id) on delete set null;
alter table taken_books add foreign key (book_id) references books(id) on delete set null;

alter table book_tag add foreign key (tag_id) references tags(id) on delete set null;
alter table book_tag add foreign key (book_id) references books(id) on delete set null;

alter table books add foreign key (category_id) references categories(id) on delete set null;

alter table books add foreign key (translator_id) references translators(id) on delete set null;

alter table books add foreign key (publisher_id) references publishers(id) on delete set null;

alter table books add foreign key (author_id) references authors(id) on delete set null;


-- Inserts
-- Employees
insert into employees (first_name, last_name, person_id, email, salary, sex, birthday) values (
  "ali",
  "aliyan",
  "0110055555",
  "ali@gmail.com",
  10000000,
  "m",
  current_timestamp
);

-- Authors
insert into authors (first_name, last_name)
  values (
    "Luke",
    "VanderHart"
);
insert into authors (first_name, last_name)
  values (
    "Ryan",
    "Neufeld"
);
insert into authors (first_name, last_name)
  values (
    "Conrad",
    "Barski M.D"
);

-- Publishers
insert into publishers (name, address, phone_number, website, email)
  values (
    "o'reilly",
    "CA, USA",
    "7078277000",
    "www.oreilly.com",
    "ca.usa@oreilly.com"
  );

-- Categories
insert into categories (name) values ("backend programming");
insert into categories (name) values ("frontend programming");
insert into categories (name) values ("system programming");
insert into categories (name) values ("micro controller programming");
insert into categories (name) values ("mobile programming");

-- Tags
insert into tags (name) values ("cool");
insert into tags (name) values ("best");
insert into tags (name) values ("hard");
insert into tags (name) values ("easy");

-- Translators
insert into translators (first_name, last_name, person_id) values (
  "ahmad",
  "ahmadi",
  "0110011111"
);

-- Books
insert into books (
  title, description, circulation, isbn, language, price, published_at, author_id, publisher_id, category_id
) values(
    "Clojure Cookbook",
    "Recipes for Functional Programming",
    "3000",
    "264d5fbf-4757-430b-9535-c81ec163d30f",
    "eng",
    12000,
    current_timestamp,
    1,
    1,
    1
  );
insert into books (
  title, description, circulation, isbn, language, price, published_at, author_id, publisher_id, category_id
) values(
    "Android Programming",
    "The big nerd ranch guide",
    "3000",
    "43b99306-60a9-4033-acc6-6ebb11f7810a",
    "eng",
    1000,
    current_timestamp,
    2,
    1,
    5
  );
insert into books (
  title, description, circulation, isbn, language, price, published_at, author_id, publisher_id, category_id
) values(
    "Land of Lisp",
    "Learn programming to LISP",
    "3000",
    "9edf0441-e967-42c6-8521-c2df1da1c53e",
    "eng",
    2000,
    current_timestamp,
    3,
    1,
    3
  );
insert into books (
  title, description, circulation, isbn, language, price, published_at, author_id, publisher_id, category_id
) values(
    "bash Cookbook",
    "Solutions and Examples for bash Users, 2nd Edition",
    "3000",
    "3943f531-36ba-4af5-93ed-e688d4c710b1",
    "eng",
    500,
    current_timestamp,
    3,
    1,
    3
  );

-- Book_Tag
insert into book_tag (book_id, tag_id) values (1, 1);
insert into book_tag (book_id, tag_id) values (1, 2);
insert into book_tag (book_id, tag_id) values (1, 3);
insert into book_tag (book_id, tag_id) values (2, 2);
insert into book_tag (book_id, tag_id) values (2, 4);
insert into book_tag (book_id, tag_id) values (3, 3);
insert into book_tag (book_id, tag_id) values (3, 3);
insert into book_tag (book_id, tag_id) values (3, 3);
insert into book_tag (book_id, tag_id) values (3, 3);

-- Members
insert into members (
  first_name,
  last_name,
  person_id,
  address,
  phone_number,
  mobile_number,
  email,
  birthday,
  sex
) values (
  "simin",
  "simini",
  "0110022222",
  "tehran, iran",
  "09122222222",
  "02122222222",
  "simin@gamil.com",
  current_timestamp,
  "f"
);
insert into members (
  first_name,
  last_name,
  person_id,
  address,
  phone_number,
  mobile_number,
  email,
  birthday,
  sex
) values (
  "hamid",
  "hamidi",
  "0110033333",
  "tehran, iran",
  "09123333333",
  "02133333333",
  "hamid@gamil.com",
  current_timestamp,
  "m"
);
insert into members (
  first_name,
  last_name,
  person_id,
  address,
  phone_number,
  mobile_number,
  email,
  birthday,
  sex
) values (
  "majid",
  "majidi",
  "0110044444",
  "tehran, iran",
  "09124444444",
  "02144444444",
  "majid@gamil.com",
  current_timestamp,
  "m"
);

-- Taken_Books
insert into taken_books (book_id, member_id) values (1, 1);
insert into taken_books (book_id, member_id) values (2, 3);
insert into taken_books (book_id, member_id) values (3, 2);
