-- tables
create table books (
  id INT primary key auto_increment,
  title varchar(30) not null,
  description text,
  circulation varchar(10),
  isbn varchar(40) unique,
  language varchar(20),
  price int(7),
  author_id int,
  publisher_id int,
  translator_id int,
  category_id int,
  published_at datetime default current_timestamp,
  created_at datetime default current_timestamp,
  updated_at datetime
);

create table authors (
  id INT primary key auto_increment,
  first_name varchar(20),
  last_name varchar(30),
  sex varchar(1),
  created_at datetime default current_timestamp
);

create table publishers (
  id INT primary key auto_increment,
  name varchar(50),
  address text,
  phone_number varchar(11) unique,
  email varchar(120) unique,
  website varchar(120) unique,
  created_at datetime default current_timestamp
);

create table translators (
  id INT primary key auto_increment,
  first_name varchar(20),
  last_name varchar(30),
  sex varchar(1),
  person_id varchar(10) unique,
  created_at datetime default current_timestamp
);

create table book_tag (
  id INT auto_increment primary key,
  book_id INT,
  tag_id INT,
  created_at datetime default current_timestamp
);

create table tags (
  id INT primary key auto_increment,
  name varchar(20) not null unique,
  created_at datetime default current_timestamp
);

create table taken_books (
  id INT primary key auto_increment,
  book_id int,
  member_id int,
  created_at datetime default current_timestamp
);

create table members (
  id INT primary key auto_increment,
  first_name varchar(20),
  last_name varchar(30),
  sex varchar(2),
  person_id varchar(11) unique,
  address text,
  phone_number varchar(12),
  mobile_number varchar(12) unique,
  email varchar(120) unique,
  birthday datetime,
  updated_at datetime,
  created_at datetime default current_timestamp
);

create table categories (
  id INT primary key auto_increment,
  name varchar(50) not null unique,
  created_at datetime default current_timestamp
);

create table employees (
  id INT primary key auto_increment,
  first_name varchar(20),
  last_name varchar(30),
  sex varchar(1),
  person_id varchar(10) unique,
  address text,
  phone_number varchar(12),
  mobile_number varchar(12) unique,
  email varchar(120) unique,
  salary int(8),
  birthday datetime,
  updated_at datetime,
  created_at datetime default current_timestamp
); 
