-- foreign keys
alter table taken_books add foreign key (member_id) references members(id) on delete set null;
alter table taken_books add foreign key (book_id) references books(id) on delete set null;

alter table book_tag add foreign key (tag_id) references tags(id) on delete set null;
alter table book_tag add foreign key (book_id) references books(id) on delete set null;

alter table books add foreign key (category_id) references categories(id) on delete set null;

alter table books add foreign key (translator_id) references translators(id) on delete set null;

alter table books add foreign key (publisher_id) references publishers(id) on delete set null;

alter table books add foreign key (author_id) references authors(id) on delete set null; 
