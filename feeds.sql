-- employees
insert into employees (first_name, last_name, person_id, email, salary, sex, birthday) values (
  "ali",
  "aliyan",
  "0110055555",
  "ali@gmail.com",
  10000000,
  "m",
  current_timestamp
);

-- authors
insert into authors (first_name, last_name)
  values (
    "Luke",
    "VanderHart"
);
insert into authors (first_name, last_name)
  values (
    "Ryan",
    "Neufeld"
);
insert into authors (first_name, last_name)
  values (
    "Conrad",
    "Barski M.D"
);

-- publishers
insert into publishers (name, address, phone_number, website, email)
  values (
    "o'reilly",
    "CA, USA",
    "7078277000",
    "www.oreilly.com",
    "ca.usa@oreilly.com"
  );

-- categories
insert into categories (name) values ("backend programming");
insert into categories (name) values ("frontend programming");
insert into categories (name) values ("system programming");
insert into categories (name) values ("micro controller programming");
insert into categories (name) values ("mobile programming");

-- tags
insert into tags (name) values ("cool");
insert into tags (name) values ("best");
insert into tags (name) values ("hard");
insert into tags (name) values ("easy");

-- translators
insert into translators (first_name, last_name, person_id) values (
  "ahmad",
  "ahmadi",
  "0110011111"
);
insert into translators (first_name, last_name, person_id) values (
  "amir ehsan",
  "rezaee",
  "0110066666"
);

-- books
insert into books (
  title, description, circulation, isbn, language, price, published_at, author_id, publisher_id, category_id
) values(
    "Clojure Cookbook",
    "Recipes for Functional Programming",
    "3000",
    "264d5fbf-4757-430b-9535-c81ec163d30f",
    "eng",
    12000,
    current_timestamp,
    1,
    1,
    1
);
insert into books (
  title, description, circulation, isbn, language, price, published_at, author_id, publisher_id, category_id
) values(
    "Android Programming",
    "The big nerd ranch guide",
    "3000",
    "43b99306-60a9-4033-acc6-6ebb11f7810a",
    "eng",
    1000,
    current_timestamp,
    2,
    1,
    5
);
insert into books (
  title, description, circulation, isbn, language, price, published_at, author_id, publisher_id, category_id
) values(
    "Land of Lisp",
    "Learn programming to LISP",
    "3000",
    "9edf0441-e967-42c6-8521-c2df1da1c53e",
    "eng",
    2000,
    current_timestamp,
    3,
    1,
    3
);
insert into books (
  title, description, circulation, isbn, language, price, published_at, author_id, publisher_id, category_id
) values(
    "bash Cookbook",
    "Solutions and Examples for bash Users, 2nd Edition",
    "3000",
    "3943f531-36ba-4af5-93ed-e688d4c710b1",
    "eng",
    500,
    current_timestamp,
    3,
    1,
    3
);

-- book_tag
insert into book_tag (book_id, tag_id) values (1, 1);
insert into book_tag (book_id, tag_id) values (1, 2);
insert into book_tag (book_id, tag_id) values (1, 3);
insert into book_tag (book_id, tag_id) values (2, 2);
insert into book_tag (book_id, tag_id) values (2, 4);
insert into book_tag (book_id, tag_id) values (3, 3);
insert into book_tag (book_id, tag_id) values (3, 3);
insert into book_tag (book_id, tag_id) values (3, 3);
insert into book_tag (book_id, tag_id) values (3, 3);

-- members
insert into members (
  first_name,
  last_name,
  person_id,
  address,
  phone_number,
  mobile_number,
  email,
  birthday,
  sex
) values (
  "simin",
  "simini",
  "0110022222",
  "tehran, iran",
  "09122222222",
  "02122222222",
  "simin@gamil.com",
  current_timestamp,
  "f"
);
insert into members (
  first_name,
  last_name,
  person_id,
  address,
  phone_number,
  mobile_number,
  email,
  birthday,
  sex
) values (
  "hamid",
  "hamidi",
  "0110033333",
  "tehran, iran",
  "09123333333",
  "02133333333",
  "hamid@gamil.com",
  current_timestamp,
  "m"
);
insert into members (
  first_name,
  last_name,
  person_id,
  address,
  phone_number,
  mobile_number,
  email,
  birthday,
  sex
) values (
  "majid",
  "majidi",
  "0110044444",
  "tehran, iran",
  "09124444444",
  "02144444444",
  "majid@gamil.com",
  current_timestamp,
  "m"
);

-- taken_books
insert into taken_books (book_id, member_id) values (1, 1);
insert into taken_books (book_id, member_id) values (2, 3);
insert into taken_books (book_id, member_id) values (3, 2);
